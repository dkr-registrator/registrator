package main

import (
	"log"
	"os"
	"testing"

	"encoding/json"

	"github.com/hashicorp/consul/api"
	"github.com/kinbiko/jsonassert"
	"github.com/stretchr/testify/assert"
)

func TestRecord(t *testing.T) {
	consul, err := api.NewClient(&api.Config{
		Address: "localhost:8500",
		Scheme:  "http",
	})

	if err != nil {
		panic(err)
	}

	tt := consul.Status()
	ldr, err := tt.Leader()
	if err != nil {
		panic(err)
	}

	log.Println(ldr)

	catalog := consul.Catalog()

	svcs, _, err := catalog.Services(nil)
	if err != nil {
		log.Printf("[WARN] consul: Error getting catalog service %s. %v", "", err)
	}

	assert.Equal(t, len(svcs), 5)

	for sr := range svcs {
		if sr == "consul" {
			continue
		}
		detailService, _, _ := consul.Catalog().Service(sr, "", nil)
		for _, value := range detailService {
			b, _ := json.MarshalIndent(value, "", "  ")
			testApiConsulResponse(t, string(b), sr, value.ID, value.ServiceID)
		}
	}

	dc := consul.Namespaces()
	_, _, err = dc.Create(&api.Namespace{
		Name: "test",
	}, &api.WriteOptions{
		Namespace: "test",
	})
	if err != nil {
		log.Println(err)
	}
}

func testApiConsulResponse(t *testing.T, consulResponse, service string, ID, ServiceID string) {
	ja := jsonassert.New(t)
	testfile, _ := os.ReadFile("test/" + service + ".json")
	// find some sort of payload
	ja.Assertf(consulResponse, string(testfile), ID, ServiceID)
}
