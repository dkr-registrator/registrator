package main

import (
	_ "gitlab.com/dkr-registrator/consul"
	_ "gitlab.com/dkr-registrator/consulkv"
	_ "gitlab.com/dkr-registrator/etcd"
	_ "gitlab.com/dkr-registrator/skydns2"
	_ "gitlab.com/dkr-registrator/zookeeper"
)
